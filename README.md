# Montevideo Flattener Script

Hello, this is the flattener script instance configured on the respective folder inside the Montevideo Backend Repo.

The code has been adapted to work in stand alone mode. So it can be cloned anywhere by anyone to get data files.

Remember to:
* Edit config.yml to add your own storage path; and...
* To create vendor folder and include dependencies, Do
```
composer install
```

Then run export.php to start content download
```
cd [clone-path]
./export.php
```

PHP 5.6 or higher is required.