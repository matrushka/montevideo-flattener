<?php

namespace Pilcrum;

class JSONDownloader extends BasicDownloader {
  public function validateHTTPResponse($response) {
    $body = $response->raw_body;
    $json = json_decode($body);

    if($json === NULL) {
      throw new APIResponseException('Response body is not valid JSON.');
    }
    else {
      return true;
    }
  }
}
