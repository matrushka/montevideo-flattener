<?php

namespace Pilcrum;

class SecratDownloader extends MontevideoDownloader {
  function get() {
    $countries = $this->getCountries();
    $downloader_config = $this->getDownloaderConfig();
    $languages = @$downloader_config['languages'];

    foreach($countries as $country) {
      $country_code = $country['code'];

      foreach($languages as $lang) {
        $country_secrat_evaluation = $this->getSecratEvaluation($country_code, $lang);
        
        $file_path = $this->getSecratOutputFileName($country_code, $lang);

        $this->saveFile($file_path, $country_secrat_evaluation);

        print_r(">> Saved secrat evaluation for country $country_code, language $lang \n");
      }
    }
  }

  function getSecratEvaluation($country_code, $lang) {
    $base_url = $this->getBaseUrl($lang);
    $url = "{$base_url}{$this->getDownloaderConfig()['endpoint']}/$country_code";

    $max_retries = $this->config->getValue('http_client')['max_retries'];
    $response = $this->httpGet($url, $max_retries);
    $secrat_evaluation = $response->raw_body;

    return $secrat_evaluation;
  }

  function getSecratOutputFileName($country_code, $lang) {
    $path = $this->getStoragePath() . "secrat/{$country_code}.{$lang}.json";
    return $path;
  }
}
