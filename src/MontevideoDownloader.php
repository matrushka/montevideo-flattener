<?php

namespace Pilcrum;

class MontevideoDownloader extends JSONDownloader {
  function getCountries() {
    // Read from file the list of countries
    // $countries_file = file_get_contents('data/countries.en.json');
    $countries_file = file_get_contents($this->getStoragePath() . 'countries.en.json');
    $countries = json_decode($countries_file, TRUE);
    return $countries;
  }

  function getContentTree($file_name = 'content_tree') {
    $content_tree_file = file_get_contents($this->getStoragePath() . $file_name . '.json');
    $content_tree = json_decode($content_tree_file, TRUE);
    return $content_tree;
  }

  function getBaseUrl($lang = 'en') {
    $api_config = $this->getApiConfig();
    $a = $api_config;
    $url = "{$a['protocol']}://{$a['host']}:{$a['port']}/$lang{$a['prefix']}";

    // Add a trailing slash if missing
    $url = (substr($url, -1) != '/') ? $url . '/' : $url;
    return $url;
  }

  function get() {
    $downloader_config = $this->getDownloaderConfig();
    $languages = @$downloader_config['languages'];

    if(!empty($languages)) {
      foreach($languages as $l) {
        $url = $this->getBaseUrl($l) . $downloader_config['endpoint'];
        
        $max_retries = $this->config->getValue('http_client')['max_retries'];
        $response = $this->httpGet($url, $max_retries);
        $body = $response->raw_body;

        $file_path = $this->getOutputFileName(".$l");
        $this->saveFile($file_path, $body);
      }
    }

    else {
      parent::get();
    }
  }
  
  function getOutputFileName($suffix = '') {
    $downloader_config = $this->getDownloaderConfig();
    if(array_key_exists('output_file', $downloader_config)) {
      $file_name = $this->getStoragePath() . $downloader_config['output_file'] . $suffix . '.json';
    }
    else {
      $file_name = $this->getStoragePath() . $downloader_config['endpoint'] . $suffix . '.json';
    }
    
    print_r('>> Outputting to: ' . $file_name . "\n");
    return $file_name;
  }
}
