<?php

namespace Pilcrum;

class SubmissionDetailsDownloader extends MontevideoDownloader {
  function get() {
    $countries = $this->getCountries();
    $content_tree = $this->getContentTree();
    $content_tree_non_evaluated = $this->getContentTree('content_tree_non_evaluated');

    $content_tree['subject_groups'] = array_merge($content_tree['subject_groups'], $content_tree_non_evaluated['subject_groups']);

    foreach($countries as $country) {
      $country_code = $country['code'];
      foreach($content_tree['subject_groups'] as $subject_group) {
        foreach($subject_group['subjects'] as $subject_nid => $subject) {
          $details = $this->getSubmissionDetails($country_code, $subject_nid);
          $file_path = $this->getSDOutputFileName($country_code, $subject_nid);
          $this->saveFile($file_path, $details);
          print_r(">> Saved submission details for country $country_code and subject $subject_nid\n");
        }
      }
    }
  }

  function getSubmissionDetails($country_code, $subject_nid) {
    $base_url = $this->getBaseUrl();
    $url = "{$base_url}{$this->getDownloaderConfig()['endpoint']}/$country_code/$subject_nid";

    $max_retries = $this->config->getValue('http_client')['max_retries'];
    $response = $this->httpGet($url, $max_retries);
    $details = $response->raw_body;

    return $details;
  }

  function getSDOutputFileName($country_code, $subject_nid) {
    $path = $this->getStoragePath() . "submission_details/country_{$country_code}_subject_{$subject_nid}.json";
    return $path;
  }
}
