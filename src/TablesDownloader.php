<?php

namespace Pilcrum;

class TablesDownloader extends MontevideoDownloader {
  function get() {
    $countries = $this->getCountries();

    foreach($countries as $country) {
      $country_code = $country['code'];
      $country_tables = $this->getTables($country_code);
      
      $file_path = $this->getTablesOutputFileName($country_code);

      $this->saveFile($file_path, $country_tables);

      print_r(">> Saved tables in for country $country_code\n");
    }
  }

  function getTables($country_code) {
    $base_url = $this->getBaseUrl();
    $url = "{$base_url}{$this->getDownloaderConfig()['endpoint']}/$country_code";

    $max_retries = $this->config->getValue('http_client')['max_retries'];
    $response = $this->httpGet($url, $max_retries);
    $table = $response->raw_body;

    return $table;
  }

  function getTablesOutputFileName($country_code) {
    $path = $this->getStoragePath() . "submission_tables/{$country_code}.json";
    return $path;
  }
}
