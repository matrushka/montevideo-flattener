<?php

namespace Pilcrum;

class SubmissionDocumentsDownloader extends MontevideoDownloader {
  function get() {
    $countries = $this->getCountries();
    $content_tree = $this->getContentTree();
    $content_tree_non_evaluated = $this->getContentTree('content_tree_non_evaluated');

    $content_tree['subject_groups'] = array_merge($content_tree['subject_groups'], $content_tree_non_evaluated['subject_groups']);

    foreach($countries as $country) {
      $country_code = $country['code'];
      foreach($content_tree['subject_groups'] as $subject_group) {
        foreach($subject_group['subjects'] as $subject_nid => $subject) {
          $documents = $this->getSubmissionDocuments($country_code, $subject_nid);
          $file_path = $this->getSDOutputFileName($country_code, $subject_nid);
          $this->saveFile($file_path, $documents);
          print_r(">> Saved submission documents for country $country_code and subject $subject_nid\n");
        }
      }
    }
  }

  function getSubmissionDocuments($country_code, $subject_nid) {
    $base_url = $this->getBaseUrl();
    $url = "{$base_url}{$this->getDownloaderConfig()['endpoint']}/$country_code/$subject_nid";

    $max_retries = $this->config->getValue('http_client')['max_retries'];
    $response = $this->httpGet($url, $max_retries);
    $documents = $response->raw_body;

    return $documents;
  }

  function getSDOutputFileName($country_code, $subject_nid) {
    $path = $this->getStoragePath() . "submission_documents/country_{$country_code}_subject_{$subject_nid}.json";
    return $path;
  }
}
